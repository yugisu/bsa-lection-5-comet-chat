export type User = {
  user: string;
  avatar: string;
};
